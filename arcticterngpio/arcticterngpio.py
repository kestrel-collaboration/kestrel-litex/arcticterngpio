#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2022 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

kB = 1024
mB = 1024*kB

# Arctic Tern GPIO interface ------------------------------------------------------------------------------

from litex.gen.common import reverse_bytes

class ArcticTernGPIO(Module, AutoCSR):
    def __init__(self, platform, pads, gpio_in, gpio_direction, gpio_out, digit_sync, clk_freq):
        # I2C bus signals
        self.i2c_sda_out = Signal()
        self.i2c_sda_direction = Signal()
        self.i2c_sda_in = Signal()
        self.i2c_scl_out = Signal()
        self.i2c_scl_direction = Signal()
        self.i2c_scl_in = Signal()

        self.specials += Instance("gpio_tca6418_bridge",
            # Parameters
            p_PERIPHERAL_CLOCK_FREQUENCY = int(clk_freq),

            # GPIO port signals
            i_gpio_in = gpio_out,
            i_gpio_direction = gpio_direction,
            o_gpio_out = gpio_in,

            # External SMD digit sync
            o_digit_sync = digit_sync,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # I2C interface
            o_sda_out = self.i2c_sda_out,
            o_sda_dir = self.i2c_sda_direction,
            i_sda_in = self.i2c_sda_in,
            o_scl_out = self.i2c_scl_out,
            o_scl_dir = self.i2c_scl_direction,
            i_scl_in = self.i2c_scl_in
        )
        # Add Verilog source files
        self.add_sources(platform)

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.sda,
            o  = self.i2c_sda_out,
            oe = self.i2c_sda_direction,
            i  = self.i2c_sda_in,
        )
        self.specials += SDRTristate(
            io = pads.scl,
            o  = self.i2c_scl_out,
            oe = self.i2c_scl_direction,
            i  = self.i2c_scl_in,
        )


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "arcticterngpio").data_location
        platform.add_source(os.path.join(vdir, "bridge.v"))
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_top.v"))
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_bit_ctrl.v"))
        platform.add_source(os.path.join(vdir, "third_party/i2c_master/i2c_master_byte_ctrl.v"))

# HACK
# If the I2C master files are already added elsewhere in the project,
# use this class to avoid re-adding them and causing a duplicate module error.
class ArcticTernGPIOI2CExists(Module, AutoCSR):
    def __init__(self, platform, pads, gpio_in, gpio_direction, gpio_out, digit_sync, clk_freq):
        # I2C bus signals
        self.i2c_sda_out = Signal()
        self.i2c_sda_direction = Signal()
        self.i2c_sda_in = Signal()
        self.i2c_scl_out = Signal()
        self.i2c_scl_direction = Signal()
        self.i2c_scl_in = Signal()

        self.specials += Instance("gpio_tca6418_bridge",
            # Parameters
            p_PERIPHERAL_CLOCK_FREQUENCY = int(clk_freq),

            # GPIO port signals
            i_gpio_in = gpio_out,
            i_gpio_direction = gpio_direction,
            o_gpio_out = gpio_in,

            # External SMD digit sync
            o_digit_sync = digit_sync,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # I2C interface
            o_sda_out = self.i2c_sda_out,
            o_sda_dir = self.i2c_sda_direction,
            i_sda_in = self.i2c_sda_in,
            o_scl_out = self.i2c_scl_out,
            o_scl_dir = self.i2c_scl_direction,
            i_scl_in = self.i2c_scl_in
        )
        # Add Verilog source files
        self.add_sources(platform)

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.sda,
            o  = self.i2c_sda_out,
            oe = self.i2c_sda_direction,
            i  = self.i2c_sda_in,
        )
        self.specials += SDRTristate(
            io = pads.scl,
            o  = self.i2c_scl_out,
            oe = self.i2c_scl_direction,
            i  = self.i2c_scl_in,
        )


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "arcticterngpio").data_location
        platform.add_source(os.path.join(vdir, "bridge.v"))

class ArcticTernSDRHexDriver(Module, AutoCSR):
    def __init__(self, platform, display_word, raw_mode, anodes, cathodes, digit_sync):
        self.specials += Instance("smd_hex_output",
            # GPIO port signals
            o_anodes = anodes,
            o_cathodes = cathodes,
            i_display_word = display_word,
            i_raw_mode = raw_mode,

            # External SMD digit sync
            i_digit_sync = digit_sync,

            # Clock
            # Put the peripheral on the main system clock
            i_clk = ClockSignal('sys')
        )
        # Add Verilog source files
        self.add_sources(platform)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "arcticterngpio").data_location
        platform.add_source(os.path.join(vdir, "smd_driver.v"))
